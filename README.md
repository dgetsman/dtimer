# DTimer

## Find Me
  * [DTimer Project/Repository](https://gitlab.com/dgetsman/dtimer)

## Description

I think it was primarily aging that demonstrated the fact that I needed some sort of application to handle multiple customized series(es?) of timers with (or without) 'break' durations between them, and/or some sort of prompting before the next one.  It really gets to be a pain in the butt trying to do ten repetitions of 15 second hamstring stretches, while trying to hold a towel to apply proper pressure backwards on the toes, with 2-3 seconds in between, while having to mess with a desktop or mobile default timer application between every round.

I know this is a lobotomized _README.md_, but I've not yet done the design process.  I'll wait on developing this more  fully until I can pull the design info right from the applicable docs.  I'm foolishly eschewing precisely what has bitten me in the buttocks on every other large application that I've developed over the past few years.  It's just for a bit, though.  I know what I'm going for, I just want to have a prototype with the basic functionality that I already understand before I break for the detailed design.
