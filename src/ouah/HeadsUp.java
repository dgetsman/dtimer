package ouah;

import java.util.LinkedList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

/**
 * Main console & entry point for DTimer.
 * 
 * @author sprite
 */
public class HeadsUp extends Application {
	private static GridPane timersPane = new GridPane();
	private static Region veil = new Region();
	
	private static boolean timersMainActive = true;

	public void start(Stage arg0) throws Exception {
		//load our definitions
		loadDefaults();
		
		//GUI controls
		Button configNewBtn = new Button("Create New Timer");
		Button editOldBtn = new Button("Edit Existing Timer");
		Button removeOldBtn = new Button("Delete Timer");
		ListView timersLvw = new ListView<String>();
		
		//button configuration
		configNewBtn.setStyle("-fx-font-weight: bold;");
		editOldBtn.setStyle("-fx-font-weight: bold;");
		removeOldBtn.setStyle("-fx-font-weight: bold;");
		
		configNewBtn.setOnAction(new ConfigTimer());
		editOldBtn.setOnAction(new EditTimer());
		removeOldBtn.setOnAction(new DeleteTimer());		
		
		//pane configuration
		timersPane.setVgap(5); timersPane.setHgap(3);
		timersPane.add(configNewBtn, 0, 0);
		timersPane.add(editOldBtn, 1, 0);
		timersPane.add(removeOldBtn, 2, 0);
		
		Scene scene = new Scene(timersPane);
		
		//misc config
		veil.setStyle("-fx-background-color: rgba(0, 0, 0, 0.3)");
		veil.setVisible(false);

		arg0.setScene(scene);
		arg0.setTitle("DTimer");
		
		arg0.show();
	}

	//getters/setters
	public static boolean isTimersMainActive() {
		return timersMainActive;
	}

	public static void setTimersMainActive(boolean timersMainActive) {
		HeadsUp.timersMainActive = timersMainActive;
	}
	
	//entry point
	public static void main(String[] args) {
		launch(args);
	}
	
	//misc methods
	public static void loadDefaults() {
		LinkedList<Timer> ouah;
		
		try {
			ouah = Options.ImportExport.loadTimerDefinitions();
		} catch (Exception ex) {
			System.out.println("Exception in loadDefaults(): " + ex.toString());
			
			return;
		}
		
		System.out.println(ouah.toString());
	}
	
	/**
	 * Visually enables/disables the primary window for user interaction.
	 */
	public static void toggleHeadsUpFunctionality() {
		timersMainActive = !timersMainActive;
		
		if (timersMainActive) {
			veil.setVisible(false);
			timersPane.setDisable(false);
		} else {
			timersPane.setDisable(true);
			veil.setVisible(true);
		}
	}

	//event handlers for buttons
	/**
	 * Handler for onAction of configNewBtn.
	 * 
	 * @author sprite
	 */
	private class ConfigTimer implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			NewTimer newTimer = new NewTimer();
			newTimer.go();
		}
	}

	/**
	 * Handler for onAction of editOldBtn.
	 * 
	 * @author sprite
	 */
	private class EditTimer implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			
		}
	}
	
	/**
	 * Handler for onAction of removeOldBtn.
	 * 
	 * @author sprite
	 */
	private class DeleteTimer implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			
		}
	}
}
