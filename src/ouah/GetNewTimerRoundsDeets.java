package ouah;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * GUI handling for dialog to enter round specific values.
 * 
 * @author sprite
 */
public class GetNewTimerRoundsDeets {
	//GUI objects
	private Scene 				roundDeetsScene;
	private Stage 				roundDeetsStage = new Stage();
	private GridPane 			roundDeetsGrid = new GridPane();
	private Label				roundHeadingLabel = new Label();
	private Label				roundIntervalLabel = new Label("Primary Duration:");
	private Label				roundInterimLabel = new Label("Interim Duration:");
	private TextField			roundIntervalTbx = new TextField();
	private TextField			roundInterimTbx = new TextField();
	private Button				nextBtn = new Button("Commit -> Next Round");
	
	//normal data
	private LinkedList<Round> 	rounds = new LinkedList<Round>();
	private int					roundCount = 0;
	
	public GetNewTimerRoundsDeets() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Entry point.
	 */
	public void go() {
		initRoundsWindow();
		
		roundDeetsStage.show();
	}
	
	/**
	 * Configures the GUI/controls.
	 */
	private void initRoundsWindow() {
		//set up the GUI, por dios
		//styles
		nextBtn.setStyle("-fx-font-weight: bold;");
		//placements/padding
		roundDeetsGrid.setVgap(5); roundDeetsGrid.setHgap(3);
		roundDeetsGrid.add(roundHeadingLabel, 0, 0, 2, 1);
		roundDeetsGrid.add(roundIntervalLabel, 0, 1);
		roundDeetsGrid.add(roundIntervalTbx, 1, 1);
		roundDeetsGrid.add(roundInterimLabel, 0, 2);
		roundDeetsGrid.add(roundInterimTbx, 1, 2);
		roundDeetsGrid.add(nextBtn, 0, 3, 2, 1);
		
		roundDeetsScene = new Scene(roundDeetsGrid);
		roundDeetsStage.setScene(roundDeetsScene);
		roundDeetsStage.setTitle(NewTimer.getTimerInCreation().getName());
		nextBtn.setOnAction(new RoundDataHandler());
		
		//control settings
		if (isInfOrSingleTimer()) {
			roundHeadingLabel.setText("Sole/Infinite Round Durations");
		} else {
			roundHeadingLabel.setText("Round " + (roundCount + 1) + " Durations");
		}
	}
	
	private boolean isInfOrSingleTimer() {
		return (NewTimer.getTimerInCreation().isInfiniteRounds() || 
				(NewTimer.getTimerInCreation().getRoundsCount() == 1));
	}
	
	/**
	 * Handler for onAction of nextBtn element.
	 * 
	 * @author sprite
	 */
	private class RoundDataHandler implements EventHandler<ActionEvent> {
		private int interval = -1, interim = -1;
		
		@Override
		public void handle(ActionEvent arg0) {
			if (!validate()) {
				Alert valAlert = new Alert(AlertType.ERROR);
				valAlert.setContentText("Round(s) values are fubar, reconfigure...");
				valAlert.show();
				
				return;
			}
			
			//save data
			rounds.add(new Round(interval, interim));
			roundCount++;
			
			//are we done?
			if (isInfOrSingleTimer()) {
				
			}
			
			//update GUI
			if (roundCount < NewTimer.getTimerInCreation().getRoundsCount()) {
				updateRoundsWindow();
			} else {
				//stuff the LinkedList into the Timer
				NewTimer.getTimerInCreation().setRounds(rounds);
				//now stuff the Timer into a LinkedList (turnabout is fair play)
				Options.addTimerToList(NewTimer.getTimerInCreation());
				
				Alert timerCompleteAlert = new Alert(AlertType.INFORMATION);
				timerCompleteAlert.setContentText("Timer successfully configured...");
				timerCompleteAlert.show();
				
				//of course we'll want to save it here, too
				try {
					Options.ImportExport.saveTimerDefinitions();
					
					NewTimer.wipeTimerInCreation();
				} catch (FileNotFoundException fnfe) {
					Alert fnfAlert = new Alert(AlertType.ERROR);
					fnfAlert.setContentText("File not found exception: " + fnfe.toString());
					fnfAlert.show();
					
					return;
				} catch (IOException ioe) {
					Alert ioAlert = new Alert(AlertType.ERROR);
					ioAlert.setContentText("IO Error: " + ioe.toString());
					ioAlert.show();
					
					return;
				}
			}
		}
		
		/**
		 * Validates user entered data for the current round.
		 * 
		 * @return boolean
		 */
		private boolean validate() {
			//integers?
			try {
				interval = Integer.parseInt(roundIntervalTbx.getText());
				interim = Integer.parseInt(roundInterimTbx.getText());
			} catch (NumberFormatException ex) {
				//no good
				return false;
			}
			
			if ((interval <= 0) || (interim <= 0)) {
				//nope
				return false;
			}
			
			//guess we're good
			return true;
		}
		
		/**
		 * Prepares the GUI form for the next round (if any).
		 */
		private void updateRoundsWindow() {
			roundHeadingLabel.setText("Round " + (roundCount + 1));
			roundIntervalTbx.setText("");
			roundInterimTbx.setText("");
		}
	}
}
