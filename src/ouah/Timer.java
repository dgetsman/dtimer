package ouah;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Timer object.
 * 
 * @author sprite
 */
public class Timer implements Serializable {
	public String 				name;
	public int					roundsCount;
	public LinkedList<Round> 	rounds;
	public boolean				infiniteRounds;
	
	//constructors
	public Timer(String name) {
		this.name = name;
		this.roundsCount = 0;
		this.rounds = new LinkedList<Round>();
		this.infiniteRounds = false;
	}
	
	public Timer() {
		this.roundsCount = 0;
		this.rounds = new LinkedList<Round>();
		this.infiniteRounds = false;
	}

	//getters/setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getRoundsCount() {
		return this.roundsCount;
	}
	
	public void setRoundsCount(int rc) {
		this.roundsCount = rc;
	}

	public LinkedList<Round> getRounds() {
		return rounds;
	}

	public void setRounds(LinkedList<Round> rounds) {
		this.rounds = rounds;
	}

	public boolean isInfiniteRounds() {
		return infiniteRounds;
	}

	public void setInfiniteRounds(boolean infiniteRounds) {
		this.infiniteRounds = infiniteRounds;
	}
}
