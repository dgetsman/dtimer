package ouah;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * New timer creation dialog GUI handling.
 * 
 * @author sprite
 */
public class NewTimer implements EventHandler<ActionEvent> {
	private Scene newTimerScene;
	private Stage newTimerStage = new Stage();
	private GridPane newTimerGrid = new GridPane();
	private Button saveExitBtn = new Button("Continue");
	private Button abandonExitBtn = new Button("Abandon Creation");
	private Label timerNameLbl = new Label("Name");
	private TextField timerNameTfl = new TextField();
	
	private static Timer timerInCreation;
	
	public NewTimer() {
		
	}

	//getters/setters
	public static Timer getTimerInCreation() {
		return timerInCreation;
	}

	public static void wipeTimerInCreation() {
		timerInCreation = null;
	}
	
	//ereh kinda the same?
	public static void setTimerInCreationRounds(int rounds) {
		timerInCreation.setRoundsCount(rounds);
	}

	/**
	 * Entry point.
	 */
	public void go() {
		initNewTimerGUI();
		
		newTimerStage.show();
	}

	@Override
	public void handle(ActionEvent arg0) {
		//restore the previous window's functionality before exit
		HeadsUp.toggleHeadsUpFunctionality();
	}
	
	/**
	 * Handles initializing the GUI elements for new timer dialog.
	 */
	private void initNewTimerGUI() {
		//take care of the previous winder
		HeadsUp.toggleHeadsUpFunctionality();
		
		//set up our winder
		//styles
		saveExitBtn.setStyle("-fx-font-weight: bold;");
		abandonExitBtn.setStyle("-fx-font-weight: bold;");
		timerNameLbl.setStyle("-fx-font-weight: bold;");
		//placements/padding
		newTimerGrid.setVgap(5); newTimerGrid.setHgap(3);
		newTimerGrid.add(timerNameLbl, 0, 0);
		newTimerGrid.add(timerNameTfl, 1, 0);
		newTimerGrid.add(saveExitBtn, 0, 1);
		newTimerGrid.add(abandonExitBtn, 1, 1);
		//get the scene/stage ready
		newTimerScene = new Scene(newTimerGrid);
		newTimerStage.setScene(newTimerScene);
		newTimerStage.setTitle("Add Timer");
		
		//bindings
		saveExitBtn.setOnAction(new SaveNExit());
	}
	
	/**
	 * Handler for SaveNExit.
	 * 
	 * @author sprite
	 */
	private class SaveNExit implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			//set up the new timer
			timerInCreation = new Timer(timerNameTfl.getText());
			
			//prepare new window
			GetNewTimerDetails newTimerDeets = new GetNewTimerDetails();
			newTimerGrid.setDisable(true);
			
			newTimerDeets.go();
		}
		
	}
}
