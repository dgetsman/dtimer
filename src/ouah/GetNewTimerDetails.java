package ouah;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * GUI handling for dialog to create a new timer.
 * 
 * @author sprite
 */
public class GetNewTimerDetails implements EventHandler<ActionEvent> {
	private Scene timerDeetsScene;
	private Stage timerDeetsStage = new Stage();
	private GridPane timerDeetsGrid = new GridPane();
	private Button saveExitBtn = new Button("Proceed");
	private Button abandonExitBtn = new Button("Abandon Creation");
	private Label roundsLabel = new Label("Rounds:");
	private Label infRoundsLabel = new Label("Infinite?");
	private Label enabledLabel = new Label("Enabled?");
	private TextField roundsTfl = new TextField();
	private CheckBox roundsEnabledCbx = new CheckBox();
	private CheckBox infRoundsCbx = new CheckBox();
	
	public GetNewTimerDetails() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Entry point.
	 */
	public void go() {
		initDeetsWindow();
		
		timerDeetsStage.show();
	}

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Initializes GUI window components.
	 */
	private void initDeetsWindow() {
		//set up the GUI, por dios
		//styles
		saveExitBtn.setStyle("-fx-font-weight: bold;");
		abandonExitBtn.setStyle("-fx-font-weight: bold;");
		//placements/padding
		timerDeetsGrid.setVgap(5); timerDeetsGrid.setHgap(3);
		timerDeetsGrid.add(roundsLabel, 0, 0);
		timerDeetsGrid.add(enabledLabel, 1, 0);
		timerDeetsGrid.add(roundsEnabledCbx, 2, 0);
		timerDeetsGrid.add(infRoundsLabel, 3, 0);
		timerDeetsGrid.add(infRoundsCbx, 4, 0);
		timerDeetsGrid.add(roundsTfl, 5, 0);
		timerDeetsGrid.add(saveExitBtn, 0, 1);
		timerDeetsGrid.add(abandonExitBtn, 5, 1);
		//control settings
		roundsEnabledCbx.setOnAction(new RoundsEnabledToggle());
		infRoundsCbx.setOnAction(new InfRoundsToggle());
		roundsTfl.setText(Options.roundsDisabledText);
		roundsTfl.setEditable(false);
		saveExitBtn.setOnAction(new GetRoundsInfo());
		
		//ready the stage/scene
		timerDeetsScene = new Scene(timerDeetsGrid);
		timerDeetsStage.setScene(timerDeetsScene);
		timerDeetsStage.setTitle(NewTimer.getTimerInCreation().getName());
	}
	
	/**
	 * Handler for onAction for roundsEnabledCbx
	 * 
	 * @author sprite
	 */
	private class RoundsEnabledToggle implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			if (roundsEnabledCbx.isSelected()) {
				//roundsTfl.setEditable(roundsEnabledCbx.isSelected());
				roundsTfl.setText("");
				roundsTfl.setEditable(true);
			} else {
				roundsTfl.setText(Options.roundsDisabledText);
				roundsTfl.setEditable(false);
				infRoundsCbx.setSelected(false);
			}
		}
	}
	
	/**
	 * Handler for onAction for infRoundsCbx.
	 * 
	 * @author sprite
	 */
	private class InfRoundsToggle implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			if (infRoundsCbx.isSelected()) {
				roundsTfl.setText(Options.roundsIrrelevantText);
				roundsTfl.setEditable(false);
			} else if (roundsEnabledCbx.isSelected()) {
				roundsTfl.setText("");
				roundsTfl.setEditable(true);
			} else {
				roundsTfl.setText(Options.roundsDisabledText);
				roundsTfl.setEditable(false);
			}
		}
	}
	
	/**
	 * Handler for onAction for save & exit.
	 * 
	 * @author sprite
	 */
	private class GetRoundsInfo implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			//validates?
			if (!validate()) {
				Alert valAlert = new Alert(AlertType.ERROR);
				valAlert.setContentText("Round(s) settings are fubar, reconfigure...");
				valAlert.show();
				
				return;
			}
			
			//NewTimer.getTimerInCreation().setRoundsCount(Integer.parseInt(roundsTfl.getText()));
			NewTimer.getTimerInCreation().setInfiniteRounds(infRoundsCbx.isSelected());
			
			//save settings
			GetNewTimerRoundsDeets timerRounds = new GetNewTimerRoundsDeets();
			timerRounds.go();
		}
		
		/**
		 * Validates information provided by the user in the roundDeets form.
		 * 
		 * @return boolean
		 */
		private boolean validate() {
			//make sure we're working with valid round specifications
			if (!roundsEnabledCbx.isSelected() && infRoundsCbx.isSelected()) {
				//nope
				return false;
			}
			
			if (!infRoundsCbx.isSelected()) {
				try {
					//store the # of rounds
					NewTimer.getTimerInCreation().setRoundsCount(Integer.parseInt(roundsTfl.getText()));
				} catch (NumberFormatException e) {
					//not an int
					return false;
				}
			}
			
			return true;
		}
	}
}
