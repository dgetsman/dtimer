package ouah;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Purely a data structure for working with user defined options, with the
 * addition of some global configuration crap until I decide to split it off.
 * 
 * @author sprite
 */
public class Options {
	//text resources; not even sure if bothering is worth it for this...
	public static final String roundsDisabledText = new String(" -=* rounds disabled *=- ");
	public static final String roundsIrrelevantText = new String(" -=* infinite *=- ");
	
	//constants
	public static final boolean generalDebugging = true;
	private static final String timerDefinitionsFN = new String(".dtimer-definitions.json");
	
	//configurable options
	public static LinkedList<Timer> timersList = new LinkedList<Timer>();
	
	//getters/setters, etc...
	public static void setTimersList(LinkedList<Timer> tList) {
		timersList = tList;
	}
	
	public static LinkedList<Timer> getTimersList() {
		return timersList;
	}
	
	public static void addTimerToList(Timer tmr) {
		timersList.add(tmr);
	}
	
	/**
	 * This is going to hold the routines necessary for serializing our timer
	 * definitions, for now.  More to come.
	 * 
	 * @author sprite
	 */
	public static class ImportExport {
		private static String homeDir = new String(System.getProperty("user.home"));
		private static File timerDefsFile = new File(homeDir + "/" + timerDefinitionsFN);
		
		/**
		 * Serializes & writes our timer definitions to the default location.
		 * 
		 * @throws FileNotFoundException 
		 */
		public static void saveTimerDefinitions() throws FileNotFoundException, IOException {
			Gson gson = new GsonBuilder().create();
			
			String jsonData = gson.toJson(timersList);
			
			//FileOutputStream fosTimerDefs = new FileOutputStream(homeDir + "/" + timerDefinitionsFN);
			//fosTimerDefs.write(jsonData);
			
			FileWriter fwTimerDefs = new FileWriter(timerDefsFile);
			/*ObjectOutputStream oosTimerDefs = new ObjectOutputStream(fosTimerDefs);*/
			
			fwTimerDefs.write(jsonData);
			fwTimerDefs.flush();
			fwTimerDefs.close();
			/*oosTimerDefs.writeObject(timersList);
			oosTimerDefs.flush();
			oosTimerDefs.close();*/
		}
		
		/**
		 * Reads serial data from the timer definitions file and packs it into
		 * the corresponding object instance.
		 * 
		 * @return LinkedList<Timer> (or null)
		 * @throws IOException
		 * @throws ClassNotFoundException
		 */
		@SuppressWarnings("unchecked")
		public static LinkedList<Timer> loadTimerDefinitions() throws IOException, ClassNotFoundException {
			Gson gson = new Gson();
			//FileInputStream fisTimerDefs = new FileInputStream(homeDir + "/" + timerDefinitionsFN);
			//ObjectInputStream oisTimerDefs = new ObjectInputStream(fisTimerDefs);
			BufferedReader brTimerDefs = new BufferedReader(new FileReader(timerDefsFile));
			
			try {
				timersList = (LinkedList<Timer>) gson.fromJson(brTimerDefs, LinkedList.class); //oisTimerDefs.readObject();
			} catch (Exception ex) {
				Alert fnfeAlert = new Alert(AlertType.INFORMATION);
				fnfeAlert.setContentText("No existing timer definitions found: " + ex.toString());
				fnfeAlert.show();
				
				return null;
			} finally {
				brTimerDefs.close();
			}
			
			return timersList;
		}
	}
}
