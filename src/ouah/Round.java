package ouah;

import java.io.Serializable;
import java.time.Duration;

/**
 * Round details object.
 * 
 * @author sprite
 */
public class Round implements Serializable {
	//public Duration activeCountdownDuration, midIntervalDuration;
	public long activeCountdownDuration, midIntervalDuration;
	
	//private short reps;	//necessary for more complex configurations; we'll add support for this later
	
	//constructor(s)
	public Round(long countdown, long interval) {
		this.activeCountdownDuration = countdown;
		this.midIntervalDuration = interval;
	}
	
	//getters/setters
	public long getActiveCountdownDuration() {
		return activeCountdownDuration;
	}
	
	public void setActiveCountdownDuration(long activeCountdownDuration) {
		this.activeCountdownDuration = activeCountdownDuration;
	}
	
	public long getMidIntervalDuration() {
		return midIntervalDuration;
	}
	
	public void setMidIntervalDuration(long midIntervalDuration) {
		this.midIntervalDuration = midIntervalDuration;
	}

	/*public short getReps() {
		return reps;
	}

	public void setReps(short reps) {
		this.reps = reps;
	}*/
}
